[![pipeline status](https://gitlab.com/neverserious/k8s-app-transmission/badges/main/pipeline.svg)](https://gitlab.com/neverserious/k8s-app-transmission/-/commits/main) 
# k8s-app-transmission

## A useful configuration to deploy transmission in Kubernetes

Disclaimer: This code is my own, for my own purpose. There is no implied warranty for its use, and you are on your own to support it if you do.
