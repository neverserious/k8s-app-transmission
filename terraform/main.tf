resource "kubernetes_namespace" "transmission" {
  metadata {
    name = var.application_namespace
  }
}

resource "kubernetes_persistent_volume_claim" "transmission-config" {
  metadata {
    name      = "${var.application_name}-config"
    namespace = var.application_namespace
    labels = {
      app = var.application_name
    }
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "${var.deploy_env == "prd" ? 4 : 1}Gi"
      }
    }
  }
}

resource "kubernetes_deployment" "transmission" {
  metadata {
    name      = var.application_name
    namespace = var.application_namespace
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = var.application_name
      }
    }
    strategy {
      type = "Recreate"
    }
    template {
      metadata {
        labels = {
          app = var.application_name
        }
      }
      spec {
        container {
          name  = var.application_name
          image = "${var.transmission_image}:${var.transmission_version}"
          env {
            name  = "PUID"
            value = var.transmission_puid
          }
          env {
            name  = "PGID"
            value = var.transmission_pgid
          }
          # env {
          #   name  = "TRANSMISSION_WEB_HOME"
          #   value = var.transmission_webhome
          # }
          env {
            name  = "TZ"
            value = var.timezone
          }
          volume_mount {
            name       = "${var.application_name}-config"
            mount_path = "/config"
          }
          volume_mount {
            name       = "${var.application_name}-torrents"
            mount_path = var.torrents_path
          }
        }
        volume {
          name = "${var.application_name}-config"
          persistent_volume_claim {
            claim_name = "${var.application_name}-config"
          }
        }
        volume {
          name = "${var.application_name}-torrents"
          nfs {
            path   = var.torrents_share
            server = var.nfs_server
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "transmission" {
  metadata {
    name      = "transmission"
    namespace = var.application_namespace
    labels = {
      app = var.application_name
    }
    annotations = {
      "metallb.universe.tf/loadBalancerIPs" = var.application_ip
      "metallb.universe.tf/allow-shared-ip" = "key-to-share-${var.application_ip}"
    }
  }
  spec {
    selector = {
      app = var.application_name
    }
    port {
      name        = "${var.application_name}-ui"
      protocol    = "TCP"
      port        = var.application_uiport
      target_port = var.application_uiport
    }
    port {
      name        = "${var.application_name}-udp"
      protocol    = "UDP"
      port        = var.application_port
      target_port = var.application_port
    }
    port {
      name        = "${var.application_name}-tcp"
      protocol    = "TCP"
      port        = var.application_port
      target_port = var.application_port
    }
    type = "LoadBalancer"
  }
}

resource "kubernetes_ingress_v1" "transmission-http" {
  metadata {
    name      = "${var.application_name}-http"
    namespace = var.application_namespace
    annotations = {
      "cert-manager.io/cluster-issuer"                     = var.site_visibility
      "nginx.ingress.kubernetes.io/force-ssl-redirect"     = "true"
      "nginx.ingress.kubernetes.io/whitelist-source-range" = var.ip_whitelist
    }
  }
  spec {
    rule {
      host = "${var.application_name}.${var.domain_name}"
      http {
        path {
          path = "/"
          backend {
            service {
              name = var.application_name
              port {
                name = "${var.application_name}-ui"
              }
            }
          }
        }
      }
    }
    tls {
      hosts = [
        "${var.application_name}.${var.domain_name}"
      ]
      secret_name = "${var.application_name}-http-tls"
    }
  }
}

module "dns" {
  source                = "git::https://gitlab.com/neverserious/terraform-module-dns.git"
  freeipa_host          = var.freeipa_host
  freeipa_username      = var.freeipa_username
  freeipa_password      = var.freeipa_password
  freeipa_insecure      = true
  domain_internalname   = var.domain_name
  record_internalname   = var.application_name
  record_internaltarget = ["${var.domain_name}."]
}
