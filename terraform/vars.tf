# Edit image properties here, to version bump or change image path.
variable "transmission_image" { default = "quay.io/linuxserver.io/transmission" }
variable "transmission_version" { default = "4.0.3-r1-ls172" }

# Leave the rest of these alone. 
variable "freeipa_host" { sensitive = true }
variable "freeipa_username" { sensitive = true }
variable "freeipa_password" { sensitive = true }

variable "kube_configpath" { sensitive = true }
variable "domain_name" { sensitive = true }
variable "application_name" { default = "transmission" }
variable "application_namespace" { default = "transmission" }
variable "application_port" { default = "9034" }
variable "application_uiport" { default = "9091" }
variable "application_ip" { sensitive = true }
variable "deploy_env" {}
variable "site_visibility" { default = "internal" }
variable "torrents_share" { sensitive = true }
variable "torrents_path" { default = "/torrents" }
variable "nfs_server" { sensitive = true }
variable "transmission_puid" { default = "1000" }
variable "transmission_pgid" { default = "1000" }
variable "transmission_webhome" { default = "/flood-for-transmission/" }
variable "timezone" { default = "America/New_York" }
variable "ip_whitelist" { sensitive = true }
